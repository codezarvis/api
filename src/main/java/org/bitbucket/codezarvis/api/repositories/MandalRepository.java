/**
 * 
 */
package org.bitbucket.codezarvis.api.repositories;

import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface MandalRepository extends CrudRepository<MandalDocument, String> {

  @View(viewName = "byName")
  MandalDocument findByName(String name);
}
