/**
 * 
 */
package org.bitbucket.codezarvis.api.repositories;

import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface VillageRepository extends CrudRepository<VillageDocument, String> {
  @View(viewName = "byName")
  VillageDocument findByName(String name);
}
