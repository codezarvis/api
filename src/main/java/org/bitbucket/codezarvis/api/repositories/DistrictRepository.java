/**
 * 
 */
package org.bitbucket.codezarvis.api.repositories;

import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface DistrictRepository extends CrudRepository<DistrictDocument, String> {


  /**
   * Create a view with name "byName" in design document "_design/districtDocument"
   * 
   * @param name
   * @return
   */
  @View(viewName = "byName")
  DistrictDocument findByName(String name);
}
