/**
 * 
 */
package org.bitbucket.codezarvis.api.tests;

import org.bitbucket.codezarvis.api.configuration.CouchbaseConfig;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.service.DistrictService;
import org.bitbucket.codezarvis.api.service.MandalService;
import org.bitbucket.codezarvis.api.service.VillageService;
import org.bitbucket.codezarvis.api.utils.ServiceUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * @author Lakshmi Prasannam
 * 
 */
public class SpringTest {

  /**
   * @param args
   */
  public static void main(String[] args) {

    @SuppressWarnings("resource")
    ApplicationContext applicationContext =
        new AnnotationConfigApplicationContext(CouchbaseConfig.class);
    DistrictRepository districtRepository =
        (DistrictRepository) applicationContext.getBean("districtRepository");
    MandalRepository mandalRepository =
        (MandalRepository) applicationContext.getBean("mandalRepository");
    VillageRepository villageRepository =
        (VillageRepository) applicationContext.getBean("villageRepository");

    DistrictService districtService = ServiceUtils.getDistrictService();
    districtService.setDistrictRepository(districtRepository);
    districtService.setMandalRepository(mandalRepository);
    districtService.setVillageRepository(villageRepository);

    // List<MandalDocument> list = districtService.findMandalsByDistrictName("Krishna");

    // for (MandalDocument mandalDocument : list) {
    // System.out.println(mandalDocument.getId() + "\t" + mandalDocument.getName());
    // }

    MandalService mandalService = ServiceUtils.getMandalService();
    mandalService.setMandalRepository(mandalRepository);
    mandalService.setVillageRepository(villageRepository);
    // System.out.println(mandalService.findById("102").getName());
    // System.out.println(mandalService.findByName("VUYYURU").getName());

    /*
     * for (VillageDocument villageDocument : mandalService.findVillagesByMandnalName("VUYYURU")) {
     * System.out.println(villageDocument.getName()); }
     */

    VillageService villageService = ServiceUtils.getVillageService();
    villageService.setVillageRepository(villageRepository);

    // System.out.println(villageService.findById(String.valueOf(200)).getName());

    System.out.println(villageService.findByName(" PENAMAKURU").getName());

  }
}
