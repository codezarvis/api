/**
 * 
 */
package org.bitbucket.codezarvis.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

/**
 * @author Lakshmi Prasannam
 * 
 */
public class ApiArguments {

  @Option(name = "-create", usage = "one time use to store data in Couchbase server")
  private boolean create;

  /**
   * @return the create
   */
  public boolean getCreate() {
    return create;
  }

  @Argument
  private final List<String> arguments = new ArrayList<String>();

}
