/**
 * 
 */
package org.bitbucket.codezarvis.api.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.bitbucket.codezarvis.api.model.District;
import org.bitbucket.codezarvis.api.model.Mandal;
import org.bitbucket.codezarvis.api.model.Village;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;

import com.google.common.collect.Sets;

/**
 * @author Lakshmi Prasannam
 * 
 */
public class Mapper {
  private static final String DISTRICT_DATA_FILE = "Districts.txt";
  private static final String MANDAL_DATA_FILE = "Mandals.txt";
  private static final String VILLAGE_DATA_FILE = "Villages.txt";

  private static Set<District> DISTRICTS = null;
  private static Set<Mandal> MANDALS = null;

  private static Mapper MAPPER = new Mapper();

  static {

    DISTRICTS = readDistricts();

    readMandals();

    readVillages();
  }

  private Mapper() {

  }

  /**
   * @return the DISTRICTS
   */
  public Set<District> getDistricts() {
    return DISTRICTS;
  }


  /**
   * @return the MANDALS
   */
  public Set<Mandal> getMandals() {
    return MANDALS;
  }

  public static Mapper getInstance() {
    return MAPPER;
  }


  private static Set<District> readDistricts() {
    DISTRICTS = Sets.newHashSet();
    ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    InputStream is = classloader.getResourceAsStream(DISTRICT_DATA_FILE);
    Reader reader = new InputStreamReader(is, Charset.forName("utf8"));

    CSVReader csvReader = new CSVReader(reader, '|', CSVParser.DEFAULT_QUOTE_CHARACTER, '\0');
    String[] line;
    try {
      line = csvReader.readNext();
      while ((line = csvReader.readNext()) != null) {
        String st[] = line[0].split("\t");
        District district = new District();
        district.setId(Integer.parseInt(st[0]));
        district.setName(st[1]);
        district.setType("district");

        DISTRICTS.add(district);
      }
      csvReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      IOUtils.closeQuietly(reader);
    }
    return DISTRICTS;

  }

  private static void readMandals() {
    MANDALS = Sets.newHashSet();

    ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    InputStream is = classloader.getResourceAsStream(MANDAL_DATA_FILE);
    Reader reader = new InputStreamReader(is, Charset.forName("utf8"));

    CSVReader csvReader = new CSVReader(reader, '|', CSVParser.DEFAULT_QUOTE_CHARACTER, '\0');
    String[] line;
    try {
      line = csvReader.readNext();
      while ((line = csvReader.readNext()) != null) {
        String st[] = line[0].split("\t");

        Mandal mandal = new Mandal();
        mandal.setId(Integer.parseInt(st[0]));
        mandal.setName(st[4]);
        mandal.setCode(st[3]);
        mandal.setType("mandal");

        for (District district : DISTRICTS) {
          if (district.getId() == Integer.parseInt(st[1])) {
            mandal.setDistrictid(district);
            district.getMandals().add(mandal);
          }
        }
        MANDALS.add(mandal);
      }
      csvReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      IOUtils.closeQuietly(reader);
    }
  }

  private static void readVillages() {
    ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    InputStream is = classloader.getResourceAsStream(VILLAGE_DATA_FILE);
    Reader reader = new InputStreamReader(is, Charset.forName("utf8"));

    CSVReader csvReader = new CSVReader(reader, '|', CSVParser.DEFAULT_QUOTE_CHARACTER, '\0');
    String[] line;
    try {
      line = csvReader.readNext();
      while ((line = csvReader.readNext()) != null) {
        String st[] = line[0].split("\t");
        for (Mandal mandal : MANDALS) {
          if (Integer.parseInt(mandal.getCode()) == Integer.parseInt(st[3])
              && mandal.getDistrictid().getId() == Integer.parseInt(st[1])) {

            Village village = new Village();
            village.setId(Integer.parseInt(st[0]));
            village.setName(st[6]);
            village.setType("village");
            village.setMandalid(mandal);
            village.setCode(st[5]);
            mandal.getVillages().add(village);
          }

        }
      }
      csvReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      IOUtils.closeQuietly(reader);
    }

  }

}
