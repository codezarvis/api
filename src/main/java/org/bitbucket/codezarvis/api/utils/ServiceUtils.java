/**
 * 
 */
package org.bitbucket.codezarvis.api.utils;

import org.bitbucket.codezarvis.api.service.DistrictService;
import org.bitbucket.codezarvis.api.service.MandalService;
import org.bitbucket.codezarvis.api.service.VillageService;
import org.bitbucket.codezarvis.api.service.impl.DistrictServiceImpl;
import org.bitbucket.codezarvis.api.service.impl.MandalServiceImpl;
import org.bitbucket.codezarvis.api.service.impl.VillageServiceImpl;

/**
 * @author Lakshmi Prasannam
 * 
 */
public class ServiceUtils {

  /**
   * @return
   */
  public static DistrictService getDistrictService() {
    return DistrictServiceImpl.getInstance();
  }

  /**
   * @return
   */
  public static MandalService getMandalService() {
    return MandalServiceImpl.getInstance();
  }

  /**
   * @return
   */
  public static VillageService getVillageService() {
    return VillageServiceImpl.getInstance();
  }
}
