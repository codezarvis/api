package org.bitbucket.codezarvis.api.model;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * 
 * @author Lakshmi Prasannam
 */
public class Mandal implements Serializable {

  private static final long serialVersionUID = 2734225104181257452L;

  private Integer id;
  private String type;
  private String name;
  private String code;
  private District districtid;
  private List<Village> villages = Lists.newArrayList();

  public Mandal() {}

  public Mandal(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public District getDistrictid() {
    return districtid;
  }

  public void setDistrictid(District districtid) {
    this.districtid = districtid;
  }

  public List<Village> getVillages() {
    return villages;
  }

  public void setVillages(List<Village> villages) {
    this.villages = villages;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Mandal)) {
      return false;
    }
    Mandal other = (Mandal) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }
}
