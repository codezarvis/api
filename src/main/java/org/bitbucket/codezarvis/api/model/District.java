package org.bitbucket.codezarvis.api.model;

import java.io.Serializable;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * 
 * @author Lakshmi Prasannam
 */

public class District implements Serializable {

  private static final long serialVersionUID = 4730867922583922701L;
  private Integer id;
  private String type;
  private String name;

  private Set<Mandal> mandals = Sets.newHashSet();

  public District() {}

  public District(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Set<Mandal> getMandals() {
    return mandals;
  }

  public void setMandals(Set<Mandal> mandals) {
    this.mandals = mandals;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof District)) {
      return false;
    }
    District other = (District) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }
}
