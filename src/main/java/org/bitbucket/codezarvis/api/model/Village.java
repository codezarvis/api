package org.bitbucket.codezarvis.api.model;

import java.io.Serializable;

/**
 * 
 * @author Lakshmi Prasannam
 */

public class Village implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2328577763768166723L;
  private Integer id;
  private String name;
  private String type;
  private String code;
  private Mandal mandalid;

  public Village() {}

  public Village(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Mandal getMandalid() {
    return mandalid;
  }

  public void setMandalid(Mandal mandalid) {
    this.mandalid = mandalid;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Village)) {
      return false;
    }
    Village other = (Village) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }
}
