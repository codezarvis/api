/**
 * 
 */
package org.bitbucket.codezarvis.api.configuration;

import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Lakshmi Prasannam
 * 
 */
public class ApiConfiguration extends Configuration {

  @JsonProperty("swagger")
  public SwaggerBundleConfiguration swaggerBundleConfiguration;
}
