/**
 * 
 */
package org.bitbucket.codezarvis.api.configuration;

import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Configuration
@EnableCouchbaseRepositories(basePackages = {"org.bitbucket.codezarvis.api.repositories"})
public class CouchbaseConfig extends AbstractCouchbaseConfiguration {

  @Override
  protected List<String> getBootstrapHosts() {
    return Collections.singletonList("127.0.0.1");
  }

  @Override
  protected String getBucketName() {
    return "default";
  }

  @Override
  protected String getBucketPassword() {
    return "";
  }

}
