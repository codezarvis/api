/**
 * 
 */
package org.bitbucket.codezarvis.api.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.math.NumberUtils;
import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.service.DistrictService;
import org.bitbucket.codezarvis.api.service.MandalService;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;


/**
 * @author Sudarsan
 * 
 */
@Path("/districts")
@Api(value = "/districts", description = "Manage District")
@Produces(MediaType.APPLICATION_JSON)
public class DistrictResource {

  private final DistrictService districtService;
  private final MandalService mandalService;

  /**
   * @param districtService
   */
  public DistrictResource(DistrictService districtService, MandalService mandalService) {
    super();
    this.districtService = districtService;
    this.mandalService = mandalService;
  }

  @GET
  @ApiOperation("List all Districts")
  public Response getAllDistricts(@Context UriInfo uriInfo) {
    URI uri = uriInfo.getBaseUriBuilder().path(DistrictResource.class).build();
    List<DistrictDocument> districts = districtService.getAll();
    return Response.status(Status.OK).entity(districts).contentLocation(uri).build();
  }

  @GET
  @ApiOperation("District by Ref, the Ref can be district ID or Name")
  @Path("/{districtRef}")
  public Response getDistrictByName(@PathParam("districtRef") String district,
      @Context UriInfo uriInfo) {
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    DistrictDocument districtDocument = null;
    if (NumberUtils.isNumber(district)) {
      districtDocument = districtService.findById(String.valueOf(Integer.parseInt(district)));
    } else {
      districtDocument = districtService.findByName(district);
    }

    if (districtDocument == null) {
      return Response.status(Status.OK).entity("District not found.").contentLocation(uri).build();
    }
    return Response.status(Status.OK).entity(districtDocument).contentLocation(uri).build();
  }

  @GET
  @ApiOperation("Mandals in a District , the Ref can be district Name")
  @Path("/{districtRef}/mandals")
  public Response getMandals(@PathParam("districtRef") String districtName, @Context UriInfo uriInfo) {
    URI uri = uriInfo.getBaseUriBuilder().path(DistrictResource.class).build();
    List<MandalDocument> mandalDocuments = districtService.findMandalsByDistrictName(districtName);
    return Response.status(Status.OK).entity(mandalDocuments).contentLocation(uri).build();
  }


  @GET
  @ApiOperation("Mandals in a District by Name , the Ref can be mandal Name")
  @Path("/{districtRef}/mandals/{mandalRef}")
  public Response getMandal(@PathParam("districtRef") String districtName,
      @PathParam("mandalRef") String mandalName, @Context UriInfo uriInfo) {
    List<MandalDocument> mandalDocuments = districtService.findMandalsByDistrictName(districtName);
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    MandalDocument mandalDocument = null;
    for (MandalDocument mandal : mandalDocuments) {
      if (mandal.getName().equalsIgnoreCase(mandalName)) {
        mandalDocument = mandal;
        break;
      }
    }
    if (mandalDocument == null) {
      return Response.status(Status.OK).entity("Mandal not found.").contentLocation(uri).build();
    }

    return Response.status(Status.OK).entity(mandalDocument).contentLocation(uri).build();
  }

  @GET
  @ApiOperation("Villages in a Mandal for a District")
  @Path("/{districtRef}/mandals/{mandalRef}/villages")
  public Response getVillages(@PathParam("districtRef") String districtName,
      @PathParam("mandalRef") String mandalName, @Context UriInfo uriInfo) {
    List<MandalDocument> mandalDocuments = districtService.findMandalsByDistrictName(districtName);
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    MandalDocument mandalDocument = null;
    for (MandalDocument mandal : mandalDocuments) {
      if (mandal.getName().equalsIgnoreCase(mandalName)) {
        mandalDocument = mandal;
        break;
      }
    }
    if (mandalDocument == null) {
      return Response.status(Status.OK).entity("Mandal not found.").contentLocation(uri).build();
    }
    List<VillageDocument> villages =
        mandalService.findVillagesByMandnalName(mandalDocument.getName());
    return Response.status(Status.OK).entity(villages).contentLocation(uri).build();
  }

  @GET
  @ApiOperation("Village in a Mandal for a District")
  @Path("/{districtRef}/mandals/{mandalRef}/villages/{villageRef}")
  public Response getVillage(@PathParam("districtRef") String districtName,
      @PathParam("mandalRef") String mandalName, @PathParam("villageRef") String villageName,
      @Context UriInfo uriInfo) {
    List<MandalDocument> mandalDocuments = districtService.findMandalsByDistrictName(districtName);
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    MandalDocument mandalDocument = null;
    for (MandalDocument mandal : mandalDocuments) {
      if (mandal.getName().equalsIgnoreCase(mandalName)) {
        mandalDocument = mandal;
        break;
      }
    }
    if (mandalDocument == null) {
      return Response.status(Status.OK).entity("Mandal not found.").contentLocation(uri).build();
    }
    List<VillageDocument> villages =
        mandalService.findVillagesByMandnalName(mandalDocument.getName());
    VillageDocument villageDocument = null;
    for (VillageDocument village : villages) {
      if (village.getName().trim().equals(villageName)) {
        villageDocument = village;
        break;
      }
    }
    return Response.status(Status.OK).entity(villageDocument).contentLocation(uri).build();
  }
}
