/**
 * 
 */
package org.bitbucket.codezarvis.api.resources;

import java.net.URI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.math.NumberUtils;
import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.service.VillageService;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @author Lakshmi Prasannam
 * 
 */

@Path("/villages")
@Api(value = "/villages", description = "Manage Village")
@Produces(MediaType.APPLICATION_JSON)
public class VillageResource {

  private final VillageService villageService;

  /**
   * @param villageRepository
   */
  public VillageResource(VillageService villageService) {
    super();
    this.villageService = villageService;
  }

  @GET
  @ApiOperation("Village by Ref, the Ref can be village ID or Name")
  @Path("/{villageRef}")
  public Response getVillagebyName(@PathParam("villageRef") String villageName,
      @Context UriInfo uriInfo) {
    VillageDocument villageDocument = villageService.findByName(villageName);
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    if (NumberUtils.isNumber(villageName)) {
      villageDocument = villageService.findById(String.valueOf(Integer.parseInt(villageName)));
    } else {
      villageDocument = villageService.findByName(villageName);
    }

    if (villageDocument == null) {
      return Response.status(Status.OK).entity("Village not found.").contentLocation(uri).build();
    }
    return Response.status(Status.OK).entity(villageDocument).contentLocation(uri).build();
  }
}
