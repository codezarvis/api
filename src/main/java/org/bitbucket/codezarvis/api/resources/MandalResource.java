/**
 * 
 */
package org.bitbucket.codezarvis.api.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.math.NumberUtils;
import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.service.MandalService;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @author Lakshmi Prasannam
 * 
 */

@Path("/mandals")
@Api(value = "/mandals", description = "Manage Mandal")
@Produces(MediaType.APPLICATION_JSON)
public class MandalResource {

  private final MandalService mandalService;

  /**
   * @param mandalService
   */
  public MandalResource(MandalService mandalService) {
    super();
    this.mandalService = mandalService;
  }

  @GET
  @ApiOperation("Mandal by Ref, the Ref can be mandal ID or Name.")
  @Path("/{mandalRef}")
  public Response getMandalByName(@PathParam("mandalRef") String mandal, @Context UriInfo uriInfo) {
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    MandalDocument mandalDocument = null;
    if (NumberUtils.isNumber(mandal)) {
      mandalDocument = mandalService.findById(String.valueOf(Integer.parseInt(mandal)));
    } else {
      mandalDocument = mandalService.findByName(mandal);
    }

    if (mandalDocument == null) {
      return Response.status(Status.OK).entity("Mandal not found.").contentLocation(uri).build();
    }
    return Response.status(Status.OK).entity(mandalDocument).contentLocation(uri).build();
  }

  @GET
  @ApiOperation("Villages by Ref, the Ref is name of the Mandal.")
  @Path("/{mandalRef}/villages")
  public List<VillageDocument> getVillagesByMandalName(@PathParam("mandalRef") String mandalName) {
    List<VillageDocument> villages = mandalService.findVillagesByMandnalName(mandalName);
    return villages;
  }

  @GET
  @ApiOperation("Village in Mandal by Ref")
  @Path("/{mandalRef}/villages/{villageRef}")
  public Response getVillageByName(@PathParam("mandalRef") String mandalName,
      @PathParam("villageRef") String villageName, @Context UriInfo uriInfo) {
    URI uri = uriInfo.getAbsolutePathBuilder().build();
    List<VillageDocument> villages = mandalService.findVillagesByMandnalName(mandalName);
    VillageDocument villageDocument = null;
    for (VillageDocument village : villages) {
      if (village.getName().trim().equals(villageName)) {
        villageDocument = village;
        break;
      }
    }
    return Response.status(Status.OK).entity(villageDocument).contentLocation(uri).build();
  }

}
