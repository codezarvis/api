package org.bitbucket.codezarvis.api.main;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import java.util.List;

import org.bitbucket.codezarvis.api.configuration.ApiConfiguration;
import org.bitbucket.codezarvis.api.configuration.CouchbaseConfig;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.resources.DistrictResource;
import org.bitbucket.codezarvis.api.resources.MandalResource;
import org.bitbucket.codezarvis.api.resources.VillageResource;
import org.bitbucket.codezarvis.api.service.DatabaseService;
import org.bitbucket.codezarvis.api.service.DistrictService;
import org.bitbucket.codezarvis.api.service.MandalService;
import org.bitbucket.codezarvis.api.service.VillageService;
import org.bitbucket.codezarvis.api.utils.ServiceUtils;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ExampleMode;
import org.kohsuke.args4j.Option;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.collect.Lists;


/**
 * @author Lakshmi Prasannam
 * 
 */
public class App extends Application<ApiConfiguration> {

  @Option(name = "-create", usage = "one time use to store data in Couchbase server")
  private boolean create;

  @Argument
  private static final List<String> arguments = Lists.newArrayList();

  private static DistrictRepository DISTRICT_REPOSITORY;
  private static MandalRepository MANDAL_REPOSITORY;
  private static VillageRepository VILLAGE_REPOSITORY;

  private static final String districtRepositoryBean = "districtRepository";
  private static final String mandalRepositoryBean = "mandalRepository";
  private static final String villageRepositoryBean = "villageRepository";


  static {
    @SuppressWarnings("resource")
    ApplicationContext applicationContext =
        new AnnotationConfigApplicationContext(CouchbaseConfig.class);
    DistrictRepository districtRepository =
        (DistrictRepository) applicationContext.getBean(districtRepositoryBean);
    MandalRepository mandalRepository =
        (MandalRepository) applicationContext.getBean(mandalRepositoryBean);
    VillageRepository villageRepository =
        (VillageRepository) applicationContext.getBean(villageRepositoryBean);

    App.DISTRICT_REPOSITORY = districtRepository;
    App.MANDAL_REPOSITORY = mandalRepository;
    App.VILLAGE_REPOSITORY = villageRepository;
  }

  public static void main(String[] args) throws Exception {

    new App().doMain(args);
    String[] serverArguments = arguments.toArray(new String[arguments.size()]);
    new App().run(serverArguments);
  }

  public void doMain(String[] args) {

    CmdLineParser parser = new CmdLineParser(this);
    parser.setUsageWidth(200);

    try {
      parser.parseArgument(args);
      if (arguments.isEmpty())
        throw new CmdLineException(parser, "No argument is given");

    } catch (CmdLineException e) {
      System.err.println(e.getMessage());
      System.err.println("java -jar App [options...] arguments...");
      parser.printUsage(System.err);
      System.err.println();
      System.err.println("  Example: java -jar App" + parser.printExample(ExampleMode.ALL));
      return;
    }

    if (create) {
      System.out
          .println("\n------------------------Started creating the Database------------------------\n");

      DatabaseService databaseService =
          new DatabaseService(DISTRICT_REPOSITORY, MANDAL_REPOSITORY, VILLAGE_REPOSITORY);

      databaseService.doWork();

      System.out
          .println("\n------------------------Completed creating the Database------------------------\n");
    }
  }



  @Override
  public void initialize(Bootstrap<ApiConfiguration> bootstrap) {
    bootstrap.addBundle(new SwaggerBundle<ApiConfiguration>() {

      @Override
      protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
          ApiConfiguration configuration) {
        return configuration.swaggerBundleConfiguration;
      }
    });

  }

  @Override
  public void run(ApiConfiguration configuration, Environment environment) throws Exception {

    DistrictService districtService = ServiceUtils.getDistrictService();
    MandalService mandalService = ServiceUtils.getMandalService();
    VillageService villageService = ServiceUtils.getVillageService();

    districtService.setDistrictRepository(DISTRICT_REPOSITORY);
    districtService.setMandalRepository(MANDAL_REPOSITORY);
    districtService.setVillageRepository(VILLAGE_REPOSITORY);

    mandalService.setMandalRepository(MANDAL_REPOSITORY);
    mandalService.setVillageRepository(VILLAGE_REPOSITORY);

    villageService.setVillageRepository(VILLAGE_REPOSITORY);

    environment.jersey().register(new DistrictResource(districtService, mandalService));
    environment.jersey().register(new MandalResource(mandalService));
    environment.jersey().register(new VillageResource(villageService));
  }

}
