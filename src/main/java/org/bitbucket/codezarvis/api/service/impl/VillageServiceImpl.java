/**
 * 
 */
package org.bitbucket.codezarvis.api.service.impl;

import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.service.VillageService;
import org.springframework.stereotype.Service;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Service
public class VillageServiceImpl implements VillageService {

  private MandalRepository mandalRepository;
  private DistrictRepository districtRepository;
  private VillageRepository villageRepository;

  private static VillageServiceImpl villageServiceImpl = new VillageServiceImpl();

  /**
   * 
   */
  private VillageServiceImpl() {}

  /**
   * @return
   */
  public static VillageService getInstance() {
    return villageServiceImpl;
  }

  @Override
  public void setDistrictRepository(DistrictRepository districtRepository) {
    this.districtRepository = districtRepository;
  }

  @Override
  public void setMandalRepository(MandalRepository mandalRepository) {
    this.mandalRepository = mandalRepository;
  }

  @Override
  public void setVillageRepository(VillageRepository villageRepository) {
    this.villageRepository = villageRepository;
  }

  @Override
  public VillageDocument findById(String villageId) {
    return villageRepository.findOne(villageId);
  }

  @Override
  public VillageDocument findByName(String villageName) {
    return villageRepository.findByName(villageName);
  }

}
