/**
 * 
 */
package org.bitbucket.codezarvis.api.service;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.model.District;
import org.bitbucket.codezarvis.api.model.Mandal;
import org.bitbucket.codezarvis.api.model.Village;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Service
public class DatabaseService {

  private final DistrictRepository districtRepository;
  private final MandalRepository mandalRepository;
  private final VillageRepository villageRepository;

  /**
   * @param districtRepository
   * @param mandalRepository
   * @param villageRepository
   */

  @Autowired
  public DatabaseService(DistrictRepository districtRepository, MandalRepository mandalRepository,
      VillageRepository villageRepository) {
    this.districtRepository = districtRepository;
    this.mandalRepository = mandalRepository;
    this.villageRepository = villageRepository;
  }


  public void doWork() {
    Set<District> districts = Mapper.getInstance().getDistricts();
    Iterator<District> districtIterator = districts.iterator();

    while (districtIterator.hasNext()) {
      District district = districtIterator.next();

      DistrictDocument districtDocument = new DistrictDocument();
      districtDocument.setId(district.getId());
      districtDocument.setType(district.getType().trim());
      districtDocument.setName(district.getName().trim());

      Set<Mandal> mandals = district.getMandals();
      List<Integer> mandalIds = Lists.newArrayList();

      for (Mandal mandal : mandals) {
        mandalIds.add(mandal.getId());

        MandalDocument mandalDocument = new MandalDocument();
        mandalDocument.setId(mandal.getId());
        mandalDocument.setName(mandal.getName().trim());
        mandalDocument.setType(mandal.getType().trim());
        mandalDocument.setMandalCode(mandal.getCode().trim());
        mandalDocument.setDistrictDocument(districtDocument);

        List<Integer> villageIds = Lists.newArrayList();
        for (Village village : mandal.getVillages()) {

          villageIds.add(village.getId());
          VillageDocument villageDocument = new VillageDocument();
          villageDocument.setId(village.getId());
          villageDocument.setName(village.getName().trim());
          villageDocument.setType(village.getType().trim());
          villageDocument.setVillageCode(village.getCode().trim());
          villageDocument.setMandalDocument(mandalDocument);
          // Adding all Villages to Couchbase server
          System.out.println("Creating Village :" + villageDocument.getName());
          villageRepository.save(villageDocument);
          System.out.println("Sucessfully created Village :" + villageDocument.getName());
        }

        mandalDocument.setVillageIds(villageIds);
        // Adding all Mandals to Couchbase server
        System.out.println("Creating Mandal :" + mandalDocument.getName());
        mandalRepository.save(mandalDocument);
        System.out.println("Sucessfully created Mandal :" + mandalDocument.getName());
      }

      districtDocument.setMandalIds(mandalIds);
      // Adding all Districts to Couchbase server
      System.out.println("Creating District :" + districtDocument.getName());
      districtRepository.save(districtDocument);
      System.out.println("Sucessfully created District :" + districtDocument.getName());

    }
  }



}
