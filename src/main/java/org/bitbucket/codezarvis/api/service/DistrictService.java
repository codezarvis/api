/**
 * 
 */
package org.bitbucket.codezarvis.api.service;

import java.util.List;

import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.bitbucket.codezarvis.api.dto.MandalDocument;


/**
 * @author Lakshmi Prasannam
 * 
 */
public interface DistrictService extends CommonService {

  DistrictDocument findById(String districtId);

  DistrictDocument findByName(String districtName);

  List<DistrictDocument> getAll();

  void createOrUpdate(DistrictDocument districtDocument);

  List<MandalDocument> findMandalsByDistrictName(String districtName);
}
