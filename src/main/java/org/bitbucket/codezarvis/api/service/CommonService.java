/**
 * 
 */
package org.bitbucket.codezarvis.api.service;

import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface CommonService {

  void setDistrictRepository(DistrictRepository districtRepository);

  void setMandalRepository(MandalRepository mandalRepository);

  void setVillageRepository(VillageRepository villageRepository);
}
