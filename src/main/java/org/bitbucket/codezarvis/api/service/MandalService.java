/**
 * 
 */
package org.bitbucket.codezarvis.api.service;

import java.util.List;

import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.dto.VillageDocument;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface MandalService extends CommonService {

  MandalDocument findById(String mandalId);

  MandalDocument findByName(String mandalName);

  List<VillageDocument> findVillagesByMandnalName(String mandalName);
}
