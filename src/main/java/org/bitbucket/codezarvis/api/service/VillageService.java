/**
 * 
 */
package org.bitbucket.codezarvis.api.service;

import org.bitbucket.codezarvis.api.dto.VillageDocument;

/**
 * @author Lakshmi Prasannam
 * 
 */
public interface VillageService extends CommonService {

  VillageDocument findById(String villageId);

  VillageDocument findByName(String villageName);
}
