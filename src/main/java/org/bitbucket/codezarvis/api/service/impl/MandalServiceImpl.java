/**
 * 
 */
package org.bitbucket.codezarvis.api.service.impl;

import java.util.List;

import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.dto.VillageDocument;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.service.MandalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Service
public class MandalServiceImpl implements MandalService {

  private MandalRepository mandalRepository;
  private DistrictRepository districtRepository;
  private VillageRepository villageRepository;

  private static MandalServiceImpl mandalServiceImpl = new MandalServiceImpl();

  /**
   * 
   */
  private MandalServiceImpl() {}

  /**
   * @return
   */
  public static MandalService getInstance() {
    return mandalServiceImpl;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.CommonService#setDistrictRepository(org.bitbucket.codezarvis
   * .api.repositories.DistrictRepository)
   */
  @Override
  @Autowired
  public void setDistrictRepository(DistrictRepository districtRepository) {
    this.districtRepository = districtRepository;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.CommonService#setMandalRepository(org.bitbucket.codezarvis
   * .api.repositories.MandalRepository)
   */
  @Override
  @Autowired
  public void setMandalRepository(MandalRepository mandalRepository) {
    this.mandalRepository = mandalRepository;
  }



  /**
   * @param villageRepository the villageRepository to set
   */
  @Override
  @Autowired
  public void setVillageRepository(VillageRepository villageRepository) {
    this.villageRepository = villageRepository;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.bitbucket.codezarvis.api.service.MandalService#findByName(java.lang.String)
   */
  @Override
  public MandalDocument findById(String mandalId) {
    return mandalRepository.findOne(mandalId);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.bitbucket.codezarvis.api.service.MandalService#findByName(java.lang.String)
   */
  @Override
  public MandalDocument findByName(String mandalName) {
    return mandalRepository.findByName(mandalName);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.MandalService#findVillagesByMandnalName(java.lang.String)
   */
  @Override
  public List<VillageDocument> findVillagesByMandnalName(String mandalName) {
    MandalDocument mandalDocument = mandalRepository.findByName(mandalName);
    List<VillageDocument> villages = Lists.newArrayList();
    if (mandalDocument != null) {
      for (Integer id : mandalDocument.getVillageIds()) {
        VillageDocument villageDocument = villageRepository.findOne(String.valueOf(id));
        villages.add(villageDocument);
      }
    } else {
      return null;
    }
    return villages;
  }

}
