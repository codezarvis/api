/**
 * 
 */
package org.bitbucket.codezarvis.api.service.impl;

import java.util.Iterator;
import java.util.List;

import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.bitbucket.codezarvis.api.dto.MandalDocument;
import org.bitbucket.codezarvis.api.repositories.DistrictRepository;
import org.bitbucket.codezarvis.api.repositories.MandalRepository;
import org.bitbucket.codezarvis.api.repositories.VillageRepository;
import org.bitbucket.codezarvis.api.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Service
public class DistrictServiceImpl implements DistrictService {

  private DistrictRepository districtRepository;
  private MandalRepository mandalRepository;
  private static final DistrictServiceImpl districtServiceImpl = new DistrictServiceImpl();


  private DistrictServiceImpl() {}

  /**
   * @param districtRepository
   */
  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.CommonService#setDistrictRepository(org.bitbucket.codezarvis
   * .api.repositories.DistrictRepository)
   */
  @Override
  @Autowired
  public void setDistrictRepository(DistrictRepository districtRepository) {
    this.districtRepository = districtRepository;
  }

  /**
   * @param mandalRepository the mandalRepository to set
   */
  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.CommonService#setMandalRepository(org.bitbucket.codezarvis
   * .api.repositories.MandalRepository)
   */
  @Override
  @Autowired
  public void setMandalRepository(MandalRepository mandalRepository) {
    this.mandalRepository = mandalRepository;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.CommonService#setVillageRepository(org.bitbucket.codezarvis
   * .api.repositories.VillageRepository)
   */
  @Override
  public void setVillageRepository(VillageRepository villageRepository) {

  }

  /**
   * @return
   */
  public static DistrictService getInstance() {
    return districtServiceImpl;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.bitbucket.codezarvis.api.service.DistrictService#findById(java.lang.String)
   */
  @Override
  public DistrictDocument findById(String districtId) {
    return districtRepository.findOne(districtId);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.bitbucket.codezarvis.api.service.DistrictService#findByName(java.lang.String)
   */
  @Override
  public DistrictDocument findByName(String districtName) {
    return districtRepository.findByName(districtName);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.bitbucket.codezarvis.api.service.DistrictService#getAll()
   */
  @Override
  public List<DistrictDocument> getAll() {
    List<DistrictDocument> districtDocuments = Lists.newArrayList();
    Iterator<DistrictDocument> districtDocumentIterator = districtRepository.findAll().iterator();
    while (districtDocumentIterator.hasNext()) {
      districtDocuments.add(districtDocumentIterator.next());
    }
    return districtDocuments;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.DistrictService#createOrUpdate(org.bitbucket.codezarvis
   * .api.dto.DistrictDocument)
   */
  @Override
  public void createOrUpdate(DistrictDocument districtDocument) {
    districtRepository.save(districtDocument);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.bitbucket.codezarvis.api.service.DistrictService#findMandalsByDistrictName(java.lang.String
   * )
   */
  @Override
  public List<MandalDocument> findMandalsByDistrictName(String districtName) {
    DistrictDocument districtDocument = districtRepository.findByName(districtName);
    List<MandalDocument> mandalDocuments = Lists.newArrayList();
    if (districtDocument != null) {
      for (Integer id : districtDocument.getMandalIds()) {
        MandalDocument mandalDocument = mandalRepository.findOne(String.valueOf(id));
        mandalDocuments.add(mandalDocument);
      }
    } else {
      return null;
    }
    return mandalDocuments;
  }

}
