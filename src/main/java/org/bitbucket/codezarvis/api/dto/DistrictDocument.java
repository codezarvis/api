/**
 * 
 */
package org.bitbucket.codezarvis.api.dto;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Document
public class DistrictDocument {
  @Id
  @Field
  private Integer id;
  @Field
  private String type;
  @Field
  private String name;
  @Field
  private List<Integer> mandalIds;

  public DistrictDocument() {}



  /**
   * @param id
   * @param type
   * @param name
   * @param mandalIds
   */
  public DistrictDocument(Integer id, String type, String name, List<Integer> mandalIds) {
    super();
    this.id = id;
    this.type = type;
    this.name = name;
    this.mandalIds = mandalIds;
  }


  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the mandalIds
   */
  public List<Integer> getMandalIds() {
    return mandalIds;
  }

  /**
   * @param mandalIds the mandalIds to set
   */
  public void setMandalIds(List<Integer> mandalIds) {
    this.mandalIds = mandalIds;
  }



  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DistrictDocument [id=" + id + ", type=" + type + ", name=" + name + ", mandalIds="
        + mandalIds + "]";
  }


}
