/**
 * 
 */
package org.bitbucket.codezarvis.api.dto;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Document
public class MandalDocument {

  @Id
  @Field
  private Integer id;
  @Field
  private String type;
  @Field
  private String name;
  @Field
  private String mandalCode;
  @Field
  private DistrictDocument districtDocument;
  @Field
  private List<Integer> villageIds;

  public MandalDocument() {}

  /**
   * @param id
   * @param type
   * @param name
   * @param mandalCode
   * @param districtDocument
   * @param villageIds
   */
  public MandalDocument(Integer id, String type, String name, String mandalCode,
      DistrictDocument districtDocument, List<Integer> villageIds) {
    super();
    this.id = id;
    this.type = type;
    this.name = name;
    this.mandalCode = mandalCode;
    this.districtDocument = districtDocument;
    this.villageIds = villageIds;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the mandalCode
   */
  public String getMandalCode() {
    return mandalCode;
  }

  /**
   * @param mandalCode the mandalCode to set
   */
  public void setMandalCode(String mandalCode) {
    this.mandalCode = mandalCode;
  }

  /**
   * @return the districtDocument
   */
  public DistrictDocument getDistrictDocument() {
    return districtDocument;
  }

  /**
   * @param districtDocument the districtDocument to set
   */
  public void setDistrictDocument(DistrictDocument districtDocument) {
    this.districtDocument = districtDocument;
  }

  /**
   * @return the villageIds
   */
  public List<Integer> getVillageIds() {
    return villageIds;
  }

  /**
   * @param villageIds the villageIds to set
   */
  public void setVillageIds(List<Integer> villageIds) {
    this.villageIds = villageIds;
  }

}
