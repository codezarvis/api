/**
 * 
 */
package org.bitbucket.codezarvis.api.dto;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

/**
 * @author Lakshmi Prasannam
 * 
 */
@Document
public class VillageDocument {
  @Id
  @Field
  private Integer id;
  @Field
  private String type;
  @Field
  private String name;
  @Field
  private String villageCode;
  @Field
  private MandalDocument mandalDocument;

  public VillageDocument() {}

  /**
   * @param id
   * @param type
   * @param name
   * @param villageCode
   * @param mandalDocument
   */
  public VillageDocument(Integer id, String type, String name, String villageCode,
      MandalDocument mandalDocument) {
    super();
    this.id = id;
    this.type = type;
    this.name = name;
    this.villageCode = villageCode;
    this.mandalDocument = mandalDocument;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the villageCode
   */
  public String getVillageCode() {
    return villageCode;
  }

  /**
   * @param villageCode the villageCode to set
   */
  public void setVillageCode(String villageCode) {
    this.villageCode = villageCode;
  }

  /**
   * @return the mandalDocument
   */
  public MandalDocument getMandalDocument() {
    return mandalDocument;
  }

  /**
   * @param mandalDocument the mandalDocument to set
   */
  public void setMandalDocument(MandalDocument mandalDocument) {
    this.mandalDocument = mandalDocument;
  }



}
