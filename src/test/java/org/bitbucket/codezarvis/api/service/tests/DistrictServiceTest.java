package org.bitbucket.codezarvis.api.service.tests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.codezarvis.api.dto.DistrictDocument;
import org.bitbucket.codezarvis.api.service.DistrictService;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author Lakshmi Prasannam
 * 
 */
public class DistrictServiceTest {

  private static DistrictService mockDistrictService;
  private static DistrictDocument districtDocument;
  private static DistrictDocument districtDocument2;

  @BeforeClass
  public static void setUp() {
    mockDistrictService = mock(DistrictService.class);

    districtDocument =
        new DistrictDocument(3, "district", "Visakhapatname", Arrays.asList(72, 73, 74));
    districtDocument2 = new DistrictDocument(6, "district", "Krishna", Arrays.asList(18, 19, 28));

    when(mockDistrictService.getAll()).thenReturn(
        Arrays.asList(districtDocument, districtDocument2));
  }

  @Test
  public void testGetAll() {
    List<DistrictDocument> districts = mockDistrictService.getAll();
    assertEquals(2, districts.size());
  }
}
